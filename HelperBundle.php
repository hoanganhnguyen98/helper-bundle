<?php

namespace HelperBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class HelperBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/helper/js/pimcore/startup.js'
        ];
    }
}