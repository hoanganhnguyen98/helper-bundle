<?php

namespace HelperBundle\Helper;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

class YamlHelper
{
    public static function read(string $path)
    {
        try {
            return Yaml::parseFile($path);
        } catch (ParseException $e) {
            throw $e;
        }
    }
}
