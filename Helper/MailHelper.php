<?php

namespace HelperBundle\Helper;

use HelperBundle\Helper\LogHelper;

class MailHelper
{
    CONST LOG_FILE_NAME = "helper_mail";

    public static function send(string $documentPath, array $params, array $emails, string $subject = null)
    {
        try {
            $document = \Pimcore\Model\Document::getByPath($documentPath);

            if ($document) {
                if (count($emails) > 0) {
                    $mail = new \Pimcore\Mail();

                    $mail->setDocument($documentPath);
                    $mail->setParams($params);

                    if ($subject) {
                        $mail->setSubject($subject);
                    }

                    foreach ($emails as $key => $address) {
                        if ($key == 0) {
                            $mail->to($address);
                        } else {
                            $mail->addTo($address);
                        }
                    }

                    $mail->send();
                } else {

                    $message = "no address to send mail";
                    LogHelper::log(self::LOG_FILE_NAME, $messagee ."\n");
                }
            } else {

                $message = "no document to send mail";
                LogHelper::log(self::LOG_FILE_NAME, $messagee ."\n");
            }
        } catch (\Throwable $e) {

            LogHelper::logError(self::LOG_FILE_NAME, (string) ($e ."\n \n"));
        }
    }
}
