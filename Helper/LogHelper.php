<?php

namespace HelperBundle\Helper;

class LogHelper
{
    /*
     * Show in /var/log/
     */
    public static function log(string $fileName, string $message)
    {
        \Pimcore\Log\Simple::log(strtolower($fileName), $message);
    }

    public static function logError(string $fileName, string $message)
    {
        self::log('error_' . $fileName, $message);
    }
}
