<?php

namespace HelperBundle\Helper;

use HelperBundle\Helper\LogHelper;

class APIHelper
{
    CONST LOG_FILE_NAME = "helper_api";

    public static function call(string $method, string $url, array $headers = [], $body = null)
    {
        try {
            $params = [
                'headers' => $headers,
                'json' => $body,
                // 'connect_timeout' => 5,
                // 'timeout' => 5
            ];
            
            $client = new \GuzzleHttp\Client();
            $call = $client->request($method, $url, $params);
        } catch (\Throwable $e) {

            LogHelper::logError(self::LOG_FILE_NAME, (string) ($e ."\n \n"));

            return [];
        }

        $statusCode = $call->getStatusCode();
        $response = json_decode($call->getBody()->getContents(), true);

        $message = "\n". strtoupper($method) ." ". $url ."\nHeaders: ". json_encode($headers) ."\nBody: ". json_encode($body) ."\nCode: ". $statusCode ."\rResponse: ". json_encode($response);
        LogHelper::log(self::LOG_FILE_NAME, $message ."\n");

        return [
            'status' => $statusCode,
            'response' => $response
        ];
    }
}
