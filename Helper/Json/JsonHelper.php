<?php

namespace HelperBundle\Helper\Json;

use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Tool;

use HelperBundle\Helper\AssetHelper;
use HelperBundle\Helper\LogHelper;
use HelperBundle\Helper\Text\PrettyText;

class JsonHelper extends AssetHelper
{
    CONST LOG_FILE_NAME = 'helper_json';

    public static function getBaseJson(object $dataObject, array $fieldDefinitions, array $hiddenFields = [], bool $allowInherit = false)
    {
        $json = [];

        try {
            foreach ($fieldDefinitions as $name => $type) {
                if ($type instanceof Data\Localizedfields) {
                    $childs = $type->getChilds();

                    foreach ($childs as $childType) {
                        $childName = $childType->getName();

                        if (!$childType->invisible && !in_array($childName, $hiddenFields)) {
                            $json[$childName] = self::getValueByType($dataObject, $childType, $allowInherit);
                        }
                    }
                } else {
                    if (!$type->invisible && !in_array($name, $hiddenFields)) {
                        $json[$type->getName()] = self::getValueByType($dataObject, $type, $allowInherit);
                    }
                }
            }
        } catch (\Throwable $e) {

            LogHelper::logError(self::LOG_FILE_NAME, (string) ($e ."\n \n"));
        }

        return $json;
    }

    public static function getValueByType($dataObject, $type, $allowInherit = false)
    {
        $getFunction = 'get' . ucfirst($type->getName());

        if ($type instanceof Data\Date) {
            $value = $dataObject->$getFunction();

            return $value ? $value->format('d-m-Y') : null;
        }

        if ($type instanceof Data\Datetime) {
            $value = $dataObject->$getFunction();

            return $value ? $value->format('d-m-Y H:i') : null;
        }

        if ($type instanceof Data\Checkbox) {
            return $dataObject->$getFunction() ?? false;
        }

        if ($type instanceof Data\Fieldcollections) {
            $data = [];

            $items = $dataObject->$getFunction() ? $dataObject->$getFunction()->getItems() : [];

            if (count($items) == 0) {
                $parent = $dataObject->getParent();

                if ($allowInherit && $parent->getType() === 'object' && $parent->getClassName() === $dataObject->getClassName()) {

                    $items = $parent->$getFunction() ? $parent->$getFunction()->getItems() : [];
                }
            }

            if (count($items) > 0) {
                foreach ($items as $item) {
                    if (method_exists($item, 'getJson')) {
                        $data[] = $item->getJson();
                    } else {
                        $data[] = FieldCollectionJson::getJson($item);
                    }
                }
            }

            return $data;
        }

        if ($type instanceof Data\Objectbricks) {
            $data = [];

            $items = $dataObject->$getFunction()->getItems();

            if (count($items) > 0) {
                foreach ($items as $item) {
                    if (method_exists($item, 'getJson')) {
                        $data[] = $item->getJson();
                    } else {
                        $data[] = BrickJson::getJson($item);
                    }
                }
            }

            return $data;
        }

        if ($type instanceof Data\Image) {
            $image = $dataObject->$getFunction();
            return $image ? self::getLink($image) : null;
        }

        if ($type instanceof Data\ImageGallery) {
            $images = [];
            $items = $dataObject->$getFunction() ? $dataObject->$getFunction()->getItems() : [];

            if (count($items) > 0) {
                foreach ($items as $item) {
                    if ($item) {
                        $hotpot = $item->getImage();
                    
                        if ($hotpot) {
                            $images[] = self::getLink($hotpot);
                        }
                    }
                }
            }

            return $images;
        }

        if ($type instanceof Data\Video) {
            $video = $dataObject->$getFunction();

            if ($video) {
                $videoType = $video->getType();

                $data = [
                    'type' => $videoType,
                    'data' => ''
                ];

                if (in_array($videoType, ['youtube', 'vimeo', 'dailymotion'])) {
                   $data['data'] = $video->getData();
                }

                if ($videoType == 'asset') {

                    if ($video->getData()) {
                        $data['data'] = Tool::getHostUrl() . $video->getData()->getFullPath();
                    }
                }

                return $data;
            }
            

            return null;
        }

        if ($type instanceof Data\ManyToManyObjectRelation) {

            $data = [];
            $items = $dataObject->$getFunction();

            if (count($items) > 0) {
                foreach ($items as $item) {
                    if (method_exists($item, 'getJson')) {
                        $data[] = $item->getJson();
                    } else {
                        $data[] = ObjectJson::getJson($item);
                    }
                }
            }

            return $data;
        }

        if ($type instanceof Data\ManyToOneRelation) {
            $item = $dataObject->$getFunction();

            if ($item) {
                if (method_exists($item, 'getJson')) {
                    return $item->getJson();
                } else {
                    return ObjectJson::getJson($item);
                }
            }
            
            return null;
        }

        if ($type instanceof Data\Link) {
            $value = $dataObject->$getFunction();

            if ($value) {
                return $value->getDirect() ?? ($value->getPath() ? Tool::getHostUrl() . $value->getPath() : '');
            }

            return null;
        }

        if ($type instanceof Data\Wysiwyg) {
            $value = $dataObject->$getFunction();

            return PrettyText::formatWysiwyg($value);
        }

        if ($type instanceof Data\RgbaColor) {
            $value = $dataObject->$getFunction();

            return $value ? $value->getHex() : null;
        }

        if ($type instanceof Data\Input
            || $type instanceof Data\Textarea
            || $type instanceof Data\Numeric
            || $type instanceof Data\Select
        ) {
            return $dataObject->$getFunction();
        }

        return $dataObject->$getFunction();
    }
}
