<?php

namespace HelperBundle\Helper;

use Pimcore\Model\Document;
use Pimcore\Model\Document\Editable;

use HelperBundle\Helper\Json\ObjectJson;
use HelperBundle\Helper\Text\PrettyText;

class DocumentHelper
{
    public static function getPageUrl(Document $page)
    {
        try {
            return $page->getPrettyUrl() ?? '/'. strtolower(PrettyText::getPretty($page->getTitle() ?? 'page')) .'~'. $page->getId();
        } catch (\Throwable $e) {
        }

        return null;
    }

    public static function getData($path)
    {
        $data = [];

        try {
            $document = Document::getByPath($path);

            if ($document instanceof Document\Link) {
                $document = Document::getByPath($document->getHref());
            }

            if ($document instanceof Document\Page || $document instanceof Document\Snippet) {
                $editables = $document->getEditables();

                foreach ($editables as $field => $editable) {
                    $data[$field] = self::getValueByType($editable);
                }
            }
        } catch (\Throwable $e) {
        }

        return $data;
    }

    public static function getValueByType($editable)
    {
        if ($editable instanceof Editable\Input
            || $editable instanceof Editable\Textarea
            || $editable instanceof Editable\Checkbox
            || $editable instanceof Editable\Select
            || $editable instanceof Editable\Multiselect
        ) {
            return $editable->getData();
        }

        if ($editable instanceof Editable\Wysiwyg) {
            return PrettyText::formatWysiwyg($editable->getData());
        }

        if ($editable instanceof Editable\Date) {
            return $editable->getData() ? $editable->getData()->format('d-m-Y') : null;
        }

        if ($editable instanceof Editable\Image) {
            return $editable->isEmpty() ?  null : AssetGetter::getLink($editable->getImage());
        }

        if ($editable instanceof Editable\Relation) {
            return $editable->isEmpty() ? null : ObjectJson::getJson($editable->getElement());
        }

        if ($editable instanceof Editable\Relations) {
            $elements = [];

            if (!$editable->isEmpty()) {
                foreach ($editable->getElements() as $element) {
                    $elements[] = ObjectJson::getJson($element);
                }
            }
            
            return $elements;
        }

        if ($editable instanceof Editable\Link) {
            $link = [
                'isPage' => false,
                'urlOrSlug' => null
            ];

            if (!$editable->isEmpty()) {
                $internal = $editable->getData()['internal'];

                $link['urlOrSlug'] = $editable->getHref();
                if ($internal) {
                    $internalType = $editable->getData()['internalType'];

                    if ($internalType == 'document') {
                        $internalId = $editable->getData()['internalId'];

                        $page = Document::getById($internalId);

                        if ($page) {
                            $link['isPage'] = true;
                            $link['urlOrSlug'] = self::getPageUrl($page);
                        }
                    }
                }
            }

            return $link;
        }

        return $editable->getData();
    }
}
