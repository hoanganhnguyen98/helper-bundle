<?php

/**
 * 
 * Các trường bắt buộc: `active`, `username`, `password`
 * Các trường cần cấu hình trong parameter.yml
 *
 */

namespace HelperBundle\Controller\Customer;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

use Pimcore\Translation\Translator;

use HelperBundle\Validator\Validator;

/**
 * @Route("/api/helper/auth")
 */
class AuthController extends BaseController
{
    protected $translator;
    protected $validator;

    public function __construct(
        Translator $translator,
        Validator $validator
    )
    {
        $this->translator = $translator;
        $this->validator = $validator;
    }

    /**
     * @Route("/login", methods={"POST"})
     */
    public function login(Request $request)
    {
        try {
            $fields = $this->getCondition(__FUNCTION__);
            $error = $this->validator->validate($fields, $request);
            if ($error) return $this->sendValidatorError($error);

            $user = $this->getByUsername($request->get('username'));

            if ($user) {

                if ($user->getActive()) {

                    if ($this->verifyPassword($user, $request->get('password'))) {

                        return $this->sendResponse(['token' => $this->getToken($user)]);
                    } else {

                        return $this->sendError('auth.password-incorrect');
                    }
                } else {
                    $token = $this->getVerifyToken($user);

                    if ($token) {
                        // gửi token
                        $this->sendVerifyToken($user);

                        return $this->sendResponse([
                            'verify' => true,
                            'message' => $this->translator->trans('auth.user-need-verify')
                        ]);
                    } else {

                        return $this->sendError('auth.user-deactived');
                    }
                }
            } else {

                return $this->sendError('auth.user-not-exist');
            }
        } catch (\Throwable $e) {
            
            return $this->sendError($e->getMessage(), 500);
        }
    }

    /**
     * @Route("/register", methods={"POST"})
     */
    public function register(Request $request)
    {
        try {
            $fields = $this->getCondition(__FUNCTION__);
            $error = $this->validator->validate($fields, $request);
            if ($error) return $this->sendValidatorError($error);

            $user = $this->getByUsername($request->get('username'));

            if ($user) {
                if (!$user->getActive()) { // nếu đã có tài khoản và chưa được xác thực
                    $token = $this->getVerifyToken($user);

                    if ($token) {
                        // gửi token
                        $this->sendVerifyToken($user);

                        return $this->sendResponse([
                            'verify' => true,
                            'message' => $this->translator->trans('auth.user-need-verify')
                        ]);
                    } 
                }

                return $this->sendError('auth.username-existed');
            }

            $params = [];

            foreach ($fields as $field => $condition) {
                $params[$field] = $request->get($field); 
            }

            $user = $this->create($params);

            if ($user) {
                $token = $this->getVerifyToken($user);

                if ($token) {
                    // gửi token
                    $this->sendVerifyToken($user);

                    return $this->sendResponse([
                        'username' => $request->get('username'),
                        'message' => $this->translator->trans('auth.register-success')
                    ]);
                }
            } else {

                return $this->sendError('auth.register-fail');
            }
        } catch (\Throwable $e) {

            return $this->sendError($e->getMessage(), 500);
        }
    }

    /**
     * @Route("/verify", methods={"POST"})
     */
    public function verify(Request $request)
    {
        try {
            $fields = $this->getCondition(__FUNCTION__);
            $error = $this->validator->validate($fields, $request);
            if ($error) return $this->sendValidatorError($error);

            $user = $this->getByUsername($request->get('username'));
            $token = $this->canVerify($user);

            if ($token) {

                if ($token == $request->get('token')) {
                    $status = $this->updateAfterVerify($user);

                    if ($status) {
                        return $this->sendResponse([
                            'message' => $this->translator->trans('auth.verify-success')
                        ]);
                    }

                    return $this->sendError('auth.verify-fail');
                }

                return $this->sendError('auth.verify-not-match');
            }

            return $this->sendError('auth.verify-fail');
        } catch (\Throwable $e) {

            return $this->sendError($e->getMessage(), 500);
        }
    }

    /**
     * @Route("/resend/verify", methods={"POST"})
     */
    public function resendVerify(Request $request)
    {
        try {
            $fields = $this->getCondition(__FUNCTION__);
            $error = $this->validator->validate($fields, $request);
            if ($error) return $this->sendValidatorError($error);

            $user = $this->getByUsername($request->get('username'));

            if ($this->canResendVerify($user)) {

                $token = $this->getVerifyToken($user);

                if ($token) {
                    // gửi token
                    $this->sendVerifyToken($user);

                    return $this->sendResponse([
                        'message' => $this->translator->trans('auth.resend-verify-success')
                    ]);
                }
            }

            return $this->sendError('auth.resend-verify-fail');
        } catch (\Throwable $e) {

            return $this->sendError($e->getMessage(), 500);
        }
    }

    /**
     * @Route("/forget", methods={"POST"})
     */
    public function forget(Request $request)
    {
        try {
            $fields = $this->getCondition(__FUNCTION__);
            $error = $this->validator->validate($fields, $request);
            if ($error) return $this->sendValidatorError($error);

            $user = $this->getByUsername($request->get('username'));

            if ($user) {

                if ($user->getActive()) {

                    $token = $this->getForgetToken($user);

                    if ($token) {
                        // gửi token
                        $this->sendForgetToken($user);

                        return $this->sendResponse([
                            'username' => $request->get('username'),
                            'message' => $this->translator->trans('auth.forget-success')
                        ]);
                    }

                    return $this->sendError('auth.forget.fail');
                } else {

                    $token = $this->getVerifyToken($user);

                    if ($token) {
                        // gửi token
                        $this->sendVerifyToken($user);

                        return $this->sendResponse([
                            'verify' => true,
                            'message' => $this->translator->trans('auth.user-need-verify')
                        ]);
                    }

                    return $this->sendError('auth.user-deactived');
                }
            }

            return $this->sendError('auth.user-not-exist');
        } catch (\Throwable $e) {

            return $this->sendError($e->getMessage(), 500);
        }
    }

    /**
     * @Route("/reset", methods={"POST"})
     */
    public function reset(Request $request)
    {
        try {
            $fields = $this->getCondition(__FUNCTION__);
            $error = $this->validator->validate($fields, $request);
            if ($error) return $this->sendValidatorError($error);

            $user = $this->getByUsername($request->get('username'));
            $token = $this->canReset($user);

            if ($token) {

                if ($token == $request->get('token')) {
                    $status = $this->updateAfterReset($user, $request->get('password'));

                    if ($status) {
                        return $this->sendResponse([
                            'message' => $this->translator->trans('auth.reset-success')
                        ]);
                    }

                    return $this->sendError('auth.reset-fail');
                }

                return $this->sendError('auth.reset-not-match');
            }

            return $this->sendError('auth.reset-fail');
        } catch (\Throwable $e) {
            
            return $this->sendError($e->getMessage(), 500);
        }
    }

    /**
     * @Route("/resend/forget", methods={"POST"})
     */
    public function resendForget(Request $request)
    {
        try {
            $fields = $this->getCondition(__FUNCTION__);
            $error = $this->validator->validate($fields, $request);
            if ($error) return $this->sendValidatorError($error);

            $user = $this->getByUsername($request->get('username'));

            if ($this->canResendForget($user)) {

                $token = $this->getForgetToken($user);

                if ($token) {
                    // gửi token
                    $this->sendForgetToken($user);

                    return $this->sendResponse([
                        'message' => $this->translator->trans('auth.resend-forget-success')
                    ]);
                }
            }

            return $this->sendError('auth.resend-forget-fail');
        } catch (\Throwable $e) {

            return $this->sendError($e->getMessage(), 500);
        }
    }
}
