<?php

namespace HelperBundle\Controller\Customer;

use Carbon\Carbon;
use Firebase\JWT\JWT;

use Pimcore\Model\DataObject;

use HelperBundle\Config\CMF\AuthConfig;
use HelperBundle\Config\CMF\AccountConfig;
use HelperBundle\Config\CMF\ConditionConfig;

class BaseController extends \HelperBundle\Controller\BaseController
{
    public function getCondition($function)
    {
        return ConditionConfig::$function();
    }

    public function create(array $params)
    {
        $authClass = AuthConfig::getClass();

        $user = new $authClass();

        $key = $params['username'] .' ('. time() .')';
        $user->setKey(\Pimcore\Model\Element\Service::getValidKey($key, 'object'));

        $path = AuthConfig::getUserParentPath();
        $folder = DataObject::getByPath($path) ?? DataObject\Service::createFolderByPath($path);
        $user->setParent($folder);

        $status = $this->save($user, $params);

        $user->setActive(false);
        $user->setPublished(true);
        $user->save();

        return $user;
    }

    public function save($user, array $params)
    {
        $authClass = AuthConfig::getClass();

        if ($user instanceof $authClass && !empty($params)) {
            foreach ($params as $field => $value) {
                $function = 'set'. ucfirst($field);

                if (method_exists($user, $function)) {
                    $user->{$function}($value);
                }
            }

            if ($user->save()) {
                return true;
            }
        }

        return false;
    }

    public function getByUsername(string $username)
    {
        $authClass = AuthConfig::getClass();

        return call_user_func_array($authClass .'::getByUsername', [$username, ['limit' => 1,'unpublished' => false]]);
    }

    public function getByAuthToken(string $token)
    {
        try {
            $decode = $this->verifyToken($token);

            if ($decode) {
                $exp = $decode->exp;

                if ($exp > time()) {
                    $authClass = AuthConfig::getClass();
                    $tokenField = AuthConfig::getFieldToken();

                    $user = call_user_func_array($authClass .'::getById', [$decode->sub, ['limit' => 1,'unpublished' => false]]);

                    if ($user && $user->getActive() && $token == $user->{'get'. ucfirst($tokenField)}()) {
                        return $user;
                    }
                }
            }
        } catch (\Throwable $e) {
        }

        return null;
    }

    public function getToken($user)
    {
        $authClass = AuthConfig::getClass();
    
        if ($user instanceof $authClass && $user->getActive()) {
            $tokenField = AuthConfig::getFieldToken();
            $token = $user->{'get'. ucfirst($tokenField)}();

            if ($token) {
                $decode = $this->verifyToken($token);

                if ($decode && $decode->exp > time()) {
                    return $token;
                }
            }

            $now = time();

            $payload = [
                'sub' => $user->getId(),
                'rol' => strtolower($user->getClassName()),
                'iat' => $now,
                'exp' => $now + (60 * 60 * AuthConfig::getTokenExpriedHours())
            ];

            $token = JWT::encode($payload, AuthConfig::getJWTKey());

            $user->{'set'. ucfirst($tokenField)}($token);
            $user->save();

            return $token;
        }

        return null;
    }

    public function verifyToken(string $token)
    {
        try {
            $key = AuthConfig::getJWTKey();

            return JWT::decode($token, $key, array('HS256'));
        } catch (\Throwable $e) {

            return false;
        }
    }

    public function canVerify($user)
    {
        $authClass = AuthConfig::getClass();
        $authTokenField = AuthConfig::getFieldToken();
        $tokenField = AuthConfig::getFieldVerifyToken();
        $timeField = AuthConfig::getFieldVerifyTime();

        $can = $user instanceof $authClass
            && !$user->getActive()
            && !$user->{'get'. ucfirst($authTokenField)}()
            && $user->{'get'. ucfirst($tokenField)}()
            && $user->{'get'. ucfirst($timeField)}()
            && $user->{'get'. ucfirst($timeField)}()->timestamp > time();

        return $can ? $user->{'get'. ucfirst($tokenField)}() : null;
    }

    public function canResendVerify($user)
    {
        $authClass = AuthConfig::getClass();
        $authTokenField = AuthConfig::getFieldToken();
        $tokenField = AuthConfig::getFieldVerifyToken();
        $timeField = AuthConfig::getFieldVerifyTime();

        $can = $user instanceof $authClass
            && !$user->getActive()
            && !$user->{'get'. ucfirst($authTokenField)}()
            && $user->{'get'. ucfirst($tokenField)}()
            && $user->{'get'. ucfirst($timeField)}();

        return $can;
    }

    public function getVerifyToken($user)
    {
        $authClass = AuthConfig::getClass();

        if ($user instanceof $authClass && !$user->getActive()) {
            $tokenField = AuthConfig::getFieldVerifyToken();
            $timeField = AuthConfig::getFieldVerifyTime();
            $expried = AuthConfig::getVerifyTokenExpriedMinutes();

            $token = $user->{'get'. ucfirst($tokenField)}();

            // có token và còn thời gian
            if ($token
                && $user->{'get'. ucfirst($timeField)}()
                && $user->{'get'. ucfirst($timeField)}()->timestamp > time()) {

                return $token;
            }

            // sinh token và lưu lại
            $token = rand(0,9) . rand(1,9) . rand(1,9) . rand(1,9) . rand(1,9) . rand(1,9);

            $user->{'set'. ucfirst($tokenField)}($token);
            $user->{'set'. ucfirst($timeField)}(Carbon::now()->addMinutes($expried));
            $user->save();

            return $token;
        }

        return null;
    }

    public function updateAfterVerify($user)
    {
        $authClass = AuthConfig::getClass();
        $tokenField = AuthConfig::getFieldVerifyToken();
        $timeField = AuthConfig::getFieldVerifyTime();

        if ($user instanceof $authClass && !$user->getActive()) {
            $params = [
                'active' => true
            ];

            $params[$tokenField] = null;
            $params[$timeField] = null;

            return $this->save($user, $params);
        }

        return false;
    }

    public function sendVerifyToken($user)
    {
        $serviceClass = AuthConfig::getServiceClass();

        $service = new $serviceClass();
        $service->{__FUNCTION__}($user);
    }

    public function canReset($user)
    {
        $authClass = AuthConfig::getClass();
        $tokenField = AuthConfig::getFieldForgetToken();
        $timeField = AuthConfig::getFieldForgetTime();

        $can = $user instanceof $authClass
            && $user->getActive()
            && $user->{'get'. ucfirst($tokenField)}()
            && $user->{'get'. ucfirst($timeField)}()
            && $user->{'get'. ucfirst($timeField)}()->timestamp > time();

        return $can ? $user->{'get'. ucfirst($tokenField)}() : null;
    }

    public function canResendForget($user)
    {
        $authClass = AuthConfig::getClass();
        $tokenField = AuthConfig::getFieldForgetToken();
        $timeField = AuthConfig::getFieldForgetTime();

        $can = $user instanceof $authClass
            && $user->getActive()
            && $user->{'get'. ucfirst($tokenField)}()
            && $user->{'get'. ucfirst($timeField)}();

        return $can;
    }

    public function getForgetToken($user)
    {
        $authClass = AuthConfig::getClass();

        if ($user instanceof $authClass && $user->getActive()) {
            $tokenField = AuthConfig::getFieldForgetToken();
            $timeField = AuthConfig::getFieldForgetTime();
            $expried = AuthConfig::getForgetTokenExpriedMinutes();

            $token = $user->{'get'. ucfirst($tokenField)}();

            // có token và còn thời gian
            if ($token
                && $user->{'get'. ucfirst($timeField)}()
                && $user->{'get'. ucfirst($timeField)}()->timestamp > time()) {

                return $token;
            }

            // sinh token và lưu lại
            $token = rand(0,9) . rand(1,9) . rand(1,9) . rand(1,9) . rand(1,9) . rand(1,9);

            $user->{'set'. ucfirst($tokenField)}($token);
            $user->{'set'. ucfirst($timeField)}(Carbon::now()->addMinutes($expried));
            $user->save();

            return $token;
        }

        return null;
    }

    public function updateAfterReset($user, string $password)
    {
        $authClass = AuthConfig::getClass();
        $authTokenField = AuthConfig::getFieldToken();
        $tokenField = AuthConfig::getFieldForgetToken();
        $timeField = AuthConfig::getFieldForgetTime();

        if ($user instanceof $authClass && $user->getActive()) {
            $params = [
                'password' => $password
            ];

            $params[$authTokenField] = null;
            $params[$tokenField] = null;
            $params[$timeField] = null;

            return $this->save($user, $params);
        }

        return false;
    }

    public function sendForgetToken($user)
    {
        $serviceClass = AuthConfig::getServiceClass();

        $service = new $serviceClass();
        $service->{__FUNCTION__}($user);
    }

    public function verifyPassword($user, $password)
    {
        if (!method_exists($user, 'getPassword')) {
            throw new \Exception("Require `password` field in auth object Class");
        }

        return password_verify($password, $user->getPassword());
    }

    public function getAvatarOldStore()
    {
        return AccountConfig::{__FUNCTION__}();
    }

    public function getAvatarParentPath()
    {
        return AccountConfig::{__FUNCTION__}();
    }
}
