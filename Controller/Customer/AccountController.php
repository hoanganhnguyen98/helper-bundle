<?php

namespace HelperBundle\Controller\Customer;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Pimcore\Translation\Translator;

use HelperBundle\Validator\Validator;
use HelperBundle\Helper\AssetHelper;
use HelperBundle\Helper\Json\ObjectJson;

/**
 * @Route("/api/v1/helper/account")
 */
class AccountController extends BaseController
{
    protected $translator;
    protected $validator;

    public function __construct(
        Translator $translator,
        Validator $validator
    )
    {
        $this->translator = $translator;
        $this->validator = $validator;
    }

    /**
     * @Route("/logout", methods={"POST"})
     */
    public function logout(Request $request)
    {
    }

    /**
     * @Route("/view", methods={"GET"})
     */
    public function view(Request $request)
    {
        try {
            $user = $this->getUser();

            if (method_exists($user, "getJson")) {
                $data = $user->getJson();
            } else {
                $hiddenFields = [];

                $data = ObjectJson::getJson($user, $hiddenFields);
            }

            return $this->sendResponse($data);
        } catch (\Throwable $e) {

            return $this->sendError($e->getMessage(), 500);
        }
    }

    /**
     * @Route("/update", methods={"POST"})
     */
    public function update(Request $request)
    {
        try {
            $fields = $this->getCondition(__FUNCTION__);

            if (count($fields) == 0) {
                return $this->sendError('account.update-not-allow');
            }

            $error = $this->validator->validate($fields, $request);
            if ($error) return $this->sendValidatorError($error);

            $params = [];
            foreach ($fields as $field => $condition) {
                $value = $request->get($field);

                if ($value !== null) {
                    $params[$field] = $value;
                }
            }

            if (count($params) == 0) {
                return $this->sendError('account.update-nothing');
            }

            $status = $this->save($this->getUser(), $params);

            if ($status) {
                return $this->sendResponse([
                    'message' => $this->translator->trans('account.update-success')
                ]);
            }

            return $this->sendError('account.update-fail');
        } catch (\Throwable $e) {
            
            return $this->sendError($e->getMessage(), 500);
        }
    }

    /**
     * @Route("/update-avatar", methods={"POST"})
     */
    public function updateAvatar(Request $request)
    {
        try {
            $fields = $this->getCondition(__FUNCTION__);
            $error = $this->validator->validate($fields, $request);
            if ($error) return $this->sendValidatorError($error);

            $image = $request->files->get('avatar');
            $user = $this->getUser();

            if ($image) {
                $name = 'avatar_'. $user->getId();
                $folderPath = $this->getAvatarParentPath() ."/". $user->getId();

                $avatar = AssetHelper::createImageFromFile($image, $name, $folderPath);

                if ($avatar) {
                    $oldAvatar = $user->getAvatar();

                    $params = [
                        'avatar' => $avatar
                    ];

                    $status = $this->save($user, $params);

                    if ($status) {
                        if ($oldAvatar && !$this->getAvatarOldStore()) {
                            $oldAvatar->delete();
                        }

                        return $this->sendResponse([
                            'message' => $this->translator->trans('account.update-avatar-success')
                        ]);
                    }
                }
            }

            return $this->sendError('account.update-avatar-fail');
        } catch (\Throwable $e) {
            
            return $this->sendError($e->getMessage(), 500);
        }
    }

    /**
     * @Route("/change", methods={"POST"})
     */
    public function change(Request $request)
    {
        try {
            $fields = $this->getCondition(__FUNCTION__);
            $error = $this->validator->validate($fields, $request);
            if ($error) return $this->sendValidatorError($error);

            $user = $this->getUser();

            if ($user->getActive()) {

                if ($this->verifyPassword($user, $request->get('old_password'))) {

                    if (!$this->verifyPassword($user, $request->get('password'))) {

                        return $this->sendError('account.change-new-password-must-be-different');
                    }

                    $params = [
                        'password' => $request->get('password')
                    ];

                    $status = $this->save($user, $params);

                    if ($status) {
                        return $this->sendResponse([
                            'message' => $this->translator->trans('account.change-password-success')
                        ]);
                    }

                    return $this->sendError('account.change-password-fail');
                } else {

                    return $this->sendError('auth.password-incorrect');
                }
            } else {

                return $this->sendError('auth.user-deactived');
            }
        } catch (\Throwable $e) {
            
            return $this->sendError($e->getMessage(), 500);
        }
    }
}
