<?php

namespace HelperBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

use Pimcore\Controller\FrontendController;
use Pimcore\Bundle\AdminBundle\HttpFoundation\JsonResponse;
use Pimcore\Translation\Translator;

use HelperBundle\Validator\Validator;
use HelperBundle\Helper\LogHelper;

class BaseController extends FrontendController
{
    CONST LOG_FILE_NAME = 'helper_500';

    protected $translator;
    protected $validator;

    public function __construct(Translator $translator, Validator $validator)
    {
        $this->translator = $translator;
        $this->validator = $validator;
    }

    /**
     * Return a success response.
     * 
     * @param array $data
     * 
     * @return JsonResponse
     */
    public function sendResponse($data = [])
    {
        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * Return an error response.
     * 
     * @param $error
     * @param int $statusCode
     * 
     * @return JsonResponse
     */
    public function sendError($error, $statusCode = Response::HTTP_BAD_REQUEST)
    {
        // log if status code = 500
        if ($statusCode == Response::HTTP_INTERNAL_SERVER_ERROR) {
            LogHelper::logError(self::LOG_FILE_NAME, (string) ($error ."\n \n"));
        } else {
            if (is_string($error)) {
                $error = ["message" => $this->translator->trans($error)];
            }
        }

        return new JsonResponse($error, $statusCode);     
    }

    /**
     * Return an error response format validator.
     * 
     * @param array $errorMessages
     * 
     * @return JsonResponse
     */
    public function sendValidatorError(array $errorMessages)
    {
        return $this->sendError($this->validator->getError($errorMessages), Response::HTTP_BAD_REQUEST);     
    }
}
