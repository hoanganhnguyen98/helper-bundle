<?php

namespace HelperBundle\Push;

use HelperBundle\Config\PushConfig;
use HelperBundle\Helper\LogHelper;
use HelperBundle\Helper\APIHelper;

class OneSignal
{
    CONST ELEMENT = 'one_signal';
    CONST API_URL = 'https://onesignal.com/api/v1/notifications';
    CONST LOG_FILE_NAME = 'helper_one_signal';

    private static function getApiUrl()
    {
        try {
            $url = PushConfig::getBaseConfig(self::ELEMENT)['api_url'];

            return $url ?? self::API_URL;
        } catch (\Throwable $e) {
        }

        return self::API_URL;
    }

    private static function getAppId()
    {
        try {
            return PushConfig::getBaseConfig(self::ELEMENT)['app_id'];
        } catch (\Throwable $e) {
            throw $e;
        }
    }

    private static function getAppKey()
    {
        try {
            return PushConfig::getBaseConfig(self::ELEMENT)['app_key'];
        } catch (\Throwable $e) {
            throw $e;
        }
    }

    public static function push(
        array $ids,
        string $title = 'Helper Title',
        string $content = 'Helper Content',
        array $langs = ['en'],
        $target_url = null
    ) {

        try {
            $apiUrl = self::getApiUrl();
            $appId = self::getAppId();
            $appKey = self::getAppKey();

            if ($apiUrl && $appId && $appKey) {
                // $segments = ['Subscribed Users'];

                $headers = [
                    'Content-Type: application/json',
                    'Authorization: Basic ' . $appKey
                ];
                $fields = [
                    // 'included_segments' => $segments,
                    'include_player_ids' => $ids,
                    'app_id' => $appId,
                    'url' => $target_url,
                    'data' => [
                        'title' => $title,
                        'content' => $content
                    ]
                ];

                $langs = array_merge(['en'], $langs); // required 'en' language

                $heading = [];
                $contents = [];
                foreach ($langs as $lang) {
                    $heading[$lang] = $title;
                    $contents[$lang] = $content;
                }

                $fields['heading'] = $heading;
                $fields['contents'] = $contents;

                APIHelper::call('POST', $apiUrl, $headers, $fields);

                $message = 'title = '. $title  .', ids = '. implode(';', $ids);
                LogHelper::log(self::LOG_FILE_NAME, $message ."\n");
            }
        } catch (\Throwable $e) {
        
            LogHelper::logError(self::LOG_FILE_NAME, (string) ($e ."\n \n"));
        }
    }
}
