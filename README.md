# Helper Bundle

**Mục lục**

[TOC]

## Yêu cầu & Lưu ý

### Customer Management Framework Bundle (CMF)

Cần cài đặt và kích hoạt CMF Bundle trước. Github: http://https//github.com/pimcore/customer-data-framework

### Customer Class

1.Khởi tạo Customer Class bằng cách sử dụng chức năng Import file tùy chọn của CMF Bundle ở đường dẫn `\vendor\pimcore\customer-management-framework-bundle\install\class_source\optional\class_Customer_export.json`

2.Các trường bắt buộc cần khởi tạo thêm bao gồm, xem thêm ở file cấu hình `Resources\config\parameter.helper.yml`:

| Tên trường (đề xuất)  | Kiểu | Mô tả |
| ------------- | ------------- | ------------- |
| authToken | Input | token xác thực sau khi đăng nhập |
| verifyToken | Input | token xác thực sau khi đăng ký |
| verifyTime | DateTime | thời điểm hết hạn của token xác thực |
| forgetToken | Input | token quên mật khẩu |
| forgetTime | DateTime | thời điểm hết hạn của token quên mật khẩu |

### Security

Cấu hình firewall ở `config\packages\security.yaml` cho Customer:

    security:
        providers:
            .....
            helper_account_firewall_provider:
                id: helper_bundle.helper_account_firewall_provider
         ......
        firewalls:
            helper_account_firewall:
                provider: helper_account_firewall_provider
                stateless:  true
                pattern:  ^(/en/|/)api/v1/hepler/account
                guard:
                    authenticators:
                        - HelperBundle\Security\TokenAuthenticator
        ......


## Cài đặt & cấu hình Helper Bundle

1.Đặt mã nguồn bundle ở đường dẫn `bundles/HelperBundle`

2.Thêm `"HelperBundle\\": "bundles/HelperBundle"` vào  `composer.json`:

    "autoload": {
        "psr-4": {
            "App\\": "src/",
            "Pimcore\\Model\\DataObject\\": "var/classes/DataObject",
            ...,
            "HelperBundle\\": "bundles/HelperBundle"
        }
    }

3.Thêm `bundles->search_paths->bundles/HelperBundle` vào `config/config.yaml`:

    pimcore:
        bundles:
            search_paths:
                - bundles/HelperBundle

4.Chạy lệnh `composer dump-autoload`

5.Kích hoạt và cài đặt bundle trong giao diện admin

## Sitemaps
### Pimcore document

https://pimcore.com/docs/pimcore/current/Development_Documentation/Tools_and_Features/Sitemaps.html

### Cài đặt & cấu hình

1.Tạo file `presta_sitemap.php` trong thư mục `config\routes`

    presta_sitemap:
        resource: "@PrestaSitemapBundle/Resources/config/routing.yml"

2.Tạo file generate PHP (xem mẫu file `HelperBundle\Sitemaps\ExampleGenerator`)

3.Cấu hình `config\services.yaml`, ví dụ với

    HelperBundle\Sitemaps\ExampleGenerator:
        arguments:
            $processors:
                - '@HelperBundle\Sitemaps\Processors\AbsoluteURLProcessor'

3.Cấu hình `config\config.yaml`

    pimcore:
        bundles:
            search_paths:
                - bundles/HelperBundle
                
        ......
                
        sitemaps:
            generators:
                app_example:
                    enabled: true
                    priority: 50
                    generator_id: HelperBundle\Sitemaps\ExampleGenerator
                    
                # Pimcore ships a default document tree generator which is enabled by default
                # but you can easily disable it here.
                pimcore_documents:
                    enabled: true

### Lệnh sinh sitemap files

Chạy lệnh `./bin/console presta:sitemaps:dump` hoặc `./bin/console presta:sitemaps:dump --base-url=https://domain.vn/` (sinh files với domain khác domain backend hiện tại)

## phpDocumentor

Generate document syntax: `./phpDocumentor.phar run -d . -t phpDocumentor/`
