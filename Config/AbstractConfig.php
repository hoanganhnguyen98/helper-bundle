<?php

namespace HelperBundle\Config;

use HelperBundle\Helper\YamlHelper;

abstract class AbstractConfig
{
    public static function getConfig(string $path, $element = null)
    {
        try {
            $config = YamlHelper::read($path);

            return $element ? $config[$element] : $config;
        } catch (\Throwable $e) {
            throw $e;
        }
    }
}
