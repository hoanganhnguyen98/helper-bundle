<?php

namespace HelperBundle\Config;

class PushConfig extends AbstractConfig
{
    CONST CONFIG_PATH = "../bundles/HelperBundle/Resources/config/services/push.yml";

    public static function getBaseConfig(string $element)
    {
        return self::getConfig(self::CONFIG_PATH, $element);
    }
}
