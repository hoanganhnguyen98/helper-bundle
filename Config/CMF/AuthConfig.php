<?php

namespace HelperBundle\Config\CMF;

class AuthConfig extends BaseConfig
{
    CONST ELEMENT = 'auth';

    CONST CUSTOMER_OBJECT_CLASS = 'Pimcore\Model\DataObject\Customer';

    CONST DEFAULT_EXPIRED_HOURS = 240;
    CONST DEFAULT_VERIFY_EXPIRED_MINUTES = 5;
    CONST DEFAULT_FORGET_EXPIRED_MINUTES = 5;
    CONST DEFAULT_USER_PARENT_PATH = '/';

    public static function getClass()
    {
        try {
            $config = self::getBaseConfig(self::ELEMENT);

            $class = $config['object_class'] ?? self::CUSTOMER_OBJECT_CLASS;

            if (class_exists($class)) {
                return $class;
            } else {
                throw new \Exception('object_class is not existed');
            }
        } catch (\Throwable $e) {
            throw $e;
        }
    }

    public static function getServiceClass()
    {
        try {
            $config = self::getBaseConfig(self::ELEMENT);

            $class = $config['service_class'];

            if (class_exists($class)) {
                return $class;
            } else {
                throw new \Exception('service_class is not existed');
            }
        } catch (\Throwable $e) {
            throw $e;
        }
    }

    public static function getFieldToken()
    {
        try {
            $config = self::getBaseConfig(self::ELEMENT);

            return $config['field']['token'];
        } catch (\Throwable $e) {
            throw $e;
        }
    }

    public static function getFieldVerifyToken()
    {
        try {
            $config = self::getBaseConfig(self::ELEMENT);

            return $config['field']['verify_token'];
        } catch (\Throwable $e) {
            throw $e;
        }
    }

    public static function getFieldVerifyTime()
    {
        try {
            $config = self::getBaseConfig(self::ELEMENT);

            return $config['field']['verify_time'];
        } catch (\Throwable $e) {
            throw $e;
        }
    }

    public static function getFieldForgetToken()
    {
        try {
            $config = self::getBaseConfig(self::ELEMENT);

            return $config['field']['forget_token'];
        } catch (\Throwable $e) {
            throw $e;
        }
    }

    public static function getFieldForgetTime()
    {
        try {
            $config = self::getBaseConfig(self::ELEMENT);

            return $config['field']['forget_time'];
        } catch (\Throwable $e) {
            throw $e;
        }
    }

    public static function getJWTKey()
    {
        try {
            $config = self::getBaseConfig(self::ELEMENT);

            return $config['jwt_key'];
        } catch (\Throwable $e) {
            throw $e;
        }
    }

    public static function getTokenExpriedHours()
    {
        try {
            $config = self::getBaseConfig(self::ELEMENT);

            $hours = (int) $config['token_expried_hours'];

            return $hours > 0 ? $hours : self::DEFAULT_EXPIRED_HOURS;
        } catch (\Throwable $e) {
        }

        return self::DEFAULT_EXPIRED_HOURS;
    }

    public static function getVerifyTokenExpriedMinutes()
    {
        try {
            $config = self::getBaseConfig(self::ELEMENT);

            $minutes = (int) $config['verify_token_expried_minutes'];

            return $minutes > 0 ? $minutes : self::DEFAULT_VERIFY_EXPIRED_MINUTES;
        } catch (\Throwable $e) {
        }

        return self::DEFAULT_VERIFY_EXPIRED_MINUTES;
    }

    public static function getForgetTokenExpriedMinutes()
    {
        try {
            $config = self::getBaseConfig(self::ELEMENT);

            $minutes = (int) $config['forget_token_expried_minutes'];

            return $minutes > 0 ? $minutes : self::DEFAULT_FORGET_EXPIRED_MINUTES;
        } catch (\Throwable $e) {
        }

        return self::DEFAULT_FORGET_EXPIRED_MINUTES;
    }

    public static function getUserParentPath()
    {
        try {
            $config = self::getBaseConfig(self::ELEMENT);

            $path = $config['user_parent_path'];

            return $path ?? self::DEFAULT_USER_PARENT_PATH;
        } catch (\Throwable $e) {
        }

        return self::DEFAULT_USER_PARENT_PATH;
    }
}
