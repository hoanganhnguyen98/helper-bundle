<?php

namespace HelperBundle\Config\CMF;

class BaseConfig extends \HelperBundle\Config\AbstractConfig
{
    CONST BASE_PATH = '../bundles/HelperBundle/Resources/config/cmf/parameters.yml';

    public static function getBaseConfig($element = null)
    {
        return self::getConfig(self::BASE_PATH, $element);
    }
}
