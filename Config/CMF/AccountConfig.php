<?php

namespace HelperBundle\Config\CMF;

class AccountConfig extends BaseConfig
{
    CONST ELEMENT = 'account';

    CONST DEFAULT_AVATAR_OLD_STORE = false;
    CONST DEFAULT_AVATAR_PARENT_PATH = '/helper_avatars';

    public static function getAvatarOldStore()
    {
        try {
            $config = self::getBaseConfig(self::ELEMENT);

            $store = (bool) $config['avatar_old_store'];

            return $store ?? self::DEFAULT_AVATAR_OLD_STORE;
        } catch (\Throwable $e) {
        }

        return self::DEFAULT_AVATAR_OLD_STORE;
    }

    public static function getAvatarParentPath()
    {
        try {
            $config = self::getBaseConfig(self::ELEMENT);

            $path = $config['avatar_parent_path'];

            return $path ?? self::DEFAULT_AVATAR_PARENT_PATH;
        } catch (\Throwable $e) {
        }

        return self::DEFAULT_AVATAR_PARENT_PATH;
    }
}
