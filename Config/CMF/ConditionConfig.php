<?php

namespace HelperBundle\Config\CMF;

class ConditionConfig extends BaseConfig
{
    CONST ELEMENT = 'condition';

    private static function getCondition($controllerName, $functionName)
    {
        try {
            $config = self::getBaseConfig(self::ELEMENT);

            return $config[$controllerName][$functionName];
        } catch (\Throwable $e) {
            throw $e;
        }
    }

    public static function register()
    {
        return self::getCondition('auth', __FUNCTION__);
    }

    public static function login()
    {
        return self::getCondition('auth', __FUNCTION__);
    }

    public static function verify()
    {
        return self::getCondition('auth', __FUNCTION__);
    }

    public static function resendVerify()
    {
        return self::getCondition('auth', __FUNCTION__);
    }

    public static function forget()
    {
        return self::getCondition('auth', __FUNCTION__);
    }

    public static function resendForget()
    {
        return self::getCondition('auth', __FUNCTION__);
    }

    public static function reset()
    {
        return self::getCondition('auth', __FUNCTION__);
    }

    public static function update()
    {
        return self::getCondition('account', __FUNCTION__);
    }

    public static function updateAvatar()
    {
        return self::getCondition('account', __FUNCTION__);
    }

    public static function change()
    {
        return self::getCondition('account', __FUNCTION__);
    }
}
